import AsyncStorage from '@react-native-async-storage/async-storage'
import {createStore, applyMiddleware} from 'redux'
import {persistStore, persistReducer} from 'redux-persist'
import thunk from 'redux-thunk'
import CombineReducer from './reducer/CombineReducer'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['firestoreReducer']
}

const persistedReducer = persistReducer(persistConfig, CombineReducer)

const Store = createStore(persistedReducer, applyMiddleware(thunk))
const Persistor = persistStore(Store)

export {Store, Persistor}