const addData = (data) => {
    return dispatch => {
        dispatch({type: 'ADD-DATA', payload:data})
    }
}

const setUser = (data) => {
    return dispatch => {
        dispatch({type: 'SET-USER', payload:data})
    }
}

const setLogin = (data) => {
    return dispatch => {
        dispatch({type: 'LOGOUT', payload: data})
    }
}
export {addData, setUser, setLogin}