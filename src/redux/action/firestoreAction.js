import firestore from '@react-native-firebase/firestore'

const getUser = (userID) => {
    return async function (dispatch){
   
        await firestore()
            .collection('Users')
            .doc(userID)
            .onSnapshot(documentSnapshot => {
                let tampung = documentSnapshot.data()
                dispatch({type:'GET-USER', payload: tampung})
            });
    }
}

const getData = (colName) => {
    return async function (dispatch){
        const isian = await firestore().collection(colName)
        .get();
        let tampung = []
        isian.docs.forEach((isi)=>{
            tampung.push(isi.data())
        })
        dispatch({type:'GET-DATA', payload:tampung})
    }
}

const setData = (data) => {
    return dispatch => {
        dispatch({type: 'SET-DATA', payload:data})
    }
}

const setTarget = (userId) => {
    return async function (dispatch){
   
        await firestore()
            .collection('Users')
            .doc(userId)
            .onSnapshot(documentSnapshot => {
                let tampung = documentSnapshot.data()
                dispatch({type:'SET-TARGET', payload: tampung})
            });
    }
}

const getChat = (docName, userId) => {
    return async function (dispatch){
   
        await firestore()
            .collection('Chats')
            .doc(docName)
            .collection('TalkWith')
            .doc(userId)
            .onSnapshot(documentSnapshot => {
                // console.log('User data: ', documentSnapshot.data());
                let tampung = documentSnapshot.data()
                dispatch({type:'GET-CHAT', payload: tampung})
            });
    }
}

const emptyChat = (data) => {
    return dispatch => {
        dispatch({type: 'EMPTY-CHAT', payload:data})
    }
}

export {getData, getUser, setData, setTarget, getChat, emptyChat}