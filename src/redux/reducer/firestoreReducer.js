const initialState = {
    user: [],
    data: {},
    target: {},
    chat: []
}

const firestoreReducer = (state = initialState, Action) => {
    switch(Action.type){
        case 'GET-DATA':
            return{
                ...state,
                user: Action.payload.filter(item => item.name != state.data.name)
            }
        case 'GET-USER':
            return{
                ...state,
                data: Action.payload
            }
        case 'SET-DATA':
            return{
                ...state,
                data: Action.payload
            }
        case 'SET-TARGET':
            return{
                ...state,
                target: Action.payload
            }
        case 'GET-CHAT':
            return{
                ...state,
                chat: Action.payload
            }
        case 'EMPTY-CHAT':
            return{
                ...state,
                chat: Action.payload
            }
        default:
            return state
    }
}

export default firestoreReducer