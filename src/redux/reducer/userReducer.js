const initialState = {
    fullName:'',
    username:'',
    password:'',
    email:'',
    users:[
        {fullName:'Akmal Ghaffari', username:'ferryakmal97', password:'123', email:'ferryakmal97@gmail.com'},
        {fullName:'Gilang Prasetyo', username:'gylank', password:'456', email:'gylank@gmail.com'},
        {fullName:'Ari Sitohang', username:'arysp', password:'789', email:'arysp@gmail.com'}
    ],
    user: {},
    isLogin: false
}

const userReducer = (state = initialState, Action) => {
    switch(Action.type){
        case 'ADD-DATA':
            return{
                ...state,
                users:[
                    ...state.users, Action.payload
                ]
            }
        case 'INPUT-FORM':
            return {
                ...state,
                inputform: Action.payload,
            }
        case 'LOGIN':
            return{
                ...state,
                isLogin: Action.payload
            }
        case 'SET-USER':
            return{
                ...state,
                user: Action.payload
            }
        case 'LOGOUT':
            return{
                ...state,
                isLogin: Action.payload
            }
        default:
            return state
    }
}

export default userReducer