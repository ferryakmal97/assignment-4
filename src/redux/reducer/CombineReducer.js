import userReducer from './userReducer'
import firestoreReducer from './firestoreReducer'
import {combineReducers} from 'redux'

const CombineReducer = combineReducers ({
    userReducer, firestoreReducer
})

export default CombineReducer