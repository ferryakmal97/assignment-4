import React, {Component, Fragment} from 'react'
import {View, TextInput, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { BackgroundColor, PrimaryColor, SecondaryColor} from './ThemeColors';

export default class InputText extends Component {
    constructor(props){
        super(props)
        this.state = {
            password : this.props.password,
            hide: true,
            focus: false
        }
    }

    render(){
        const color = this.state.focus?SecondaryColor:BackgroundColor
        const props = this.props
        return(
            <Fragment>
                {!this.state.password && (
                    <View style={styles.inputan}>
                        <Icon name={props.iconName} style={styles.icon} color={color} />
                        <TextInput {...props} onBlur={() => this.setState({focus:false})} onFocus={() => this.setState({focus: true})} style={styles.inputTextStyle}/>
                    </View>
                )}
                {this.state.password && (
                    <View style={styles.inputText}>
                        <View style={{flexDirection:'row'}}>
                            <Icon name={props.iconName} style={styles.icon} color={color} />
                            <TextInput onBlur={() => this.setState({focus:false})} onFocus={() => this.setState({focus: true})} {...props} style={styles.inputTextStyle} secureTextEntry={this.state.hide}/>
                        </View>

                        {this.state.hide && (
                            <Icon name='eye-slash' style={styles.iconPassword} onPress={() => this.setState({hide: !this.state.hide})} color={color}/>
                        )}
                        {!this.state.hide && (
                            <Icon name='eye' style={styles.iconPassword} onPress={() => this.setState({hide: !this.state.hide})} color={color}/>
                        )}
                        
                    </View>
                )}
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    inputan:{
        flexDirection:'row',
        backgroundColor:PrimaryColor,
        marginBottom:20,
        borderRadius:30,
        paddingHorizontal:15,
        elevation:5
    },
    inputText:{
        flexDirection:'row',
        backgroundColor:PrimaryColor,
        marginBottom:20,
        borderRadius:30,
        paddingHorizontal:15,
        elevation:5,
        justifyContent:'space-between'
      },
      icon: {
        fontSize:28,
        marginHorizontal:7,
        alignSelf:'center',
        width:37
      },
      iconPassword: {
        fontSize:28,
        marginHorizontal:7,
        alignSelf:'center',
        width:37
      },
      inputTextStyle: {
        fontSize:18,
        color:BackgroundColor,
      }
})