import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { PrimaryColor } from './ThemeColors'

export default class BubbleSend extends Component {
    render() {
        const props = this.props
        return (
            <View style={styles.bubbleText}>
                <Text>{props.value}</Text>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:2}}></View>
                    <Text style={styles.time}>{props.time}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bubbleText:{
        backgroundColor:PrimaryColor,
        borderWidth:2,
        borderColor:'white',
        paddingVertical:5,
        paddingHorizontal:10,
        marginHorizontal:10,
        marginTop:10,
        alignSelf:'flex-end',
        borderRadius:10,
    },
    time:{
        alignItems:'flex-end',
    }
})
