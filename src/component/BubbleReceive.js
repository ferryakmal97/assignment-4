import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { PrimaryColor } from './ThemeColors'

export default class BubbleReceive extends Component {
    render() {
        const props = this.props
        return (
            <View style={styles.bubbleText}>
                <Text>{props.value}</Text>
                <View style={{flexDirection:'row'}}>
                    <View style={{flex:2}}></View>
                    <Text style={styles.time}>{props.time}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    bubbleText:{
        backgroundColor:'white',
        borderWidth:2,
        borderColor:PrimaryColor,
        paddingVertical:5,
        paddingHorizontal:10,
        marginHorizontal:10,
        marginTop:5,
        alignSelf:'flex-start',
        borderRadius:10,
    },
    time:{
        alignItems:'flex-end',
    }
})
