import React, {Component} from 'react'
import {View, Image, StyleSheet} from 'react-native'

class ImageProfile extends Component {
    render(){
        const props = this.props
        return(
            <View>
                <Image style={styles.photoProfile} {...props} />
            </View>
        )
    }
}

export default ImageProfile

const styles = StyleSheet.create({
    photoProfile: {
        width:50,
        height:50,
        borderRadius:100,
        marginHorizontal:15,
    }
})