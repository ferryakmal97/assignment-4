import React, {Component} from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import SplashScreen from './pages/SplashScreen';
import LoginScreen from './pages/LoginScreen';
import RegisterScreen from './pages/RegisterScreen';
import HomeScreen from './pages/HomeScreen';
import WelcomeScreen from './pages/WelcomeScreen';
import ChatScreen from './pages/ChatScreen';
import ContactScreen from './pages/ContactScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { BackgroundColor } from './component/ThemeColors';

const Stack = createStackNavigator();

const hide = {headerShown: false};

const Tab = createBottomTabNavigator();

const Dashboard = () => {
  return(
    <Tab.Navigator initialRouteName="Home" screenOptions={({ route }) => ({
      tabBarIcon: ({ focused}) => {
          let iconName;

          if (route.name === 'Home') {
          iconName = focused
              ? 'home-circle'
              : 'home-circle-outline';
          }  else {
          iconName = focused 
              ? 'contacts' 
              : 'contacts-outline';
          }
          // You can return any component that you like here!
          return <Icon name={iconName} size={30} color={BackgroundColor} />;
      },
      })}>
      <Tab.Screen name="Home" component={HomeScreen}/>
      <Tab.Screen name="Contact" component={ContactScreen}/>
    </Tab.Navigator>
  )
}

class Router extends Component {
  render(){
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" component={SplashScreen} options={hide}/>
          <Stack.Screen name="Welcome" component={WelcomeScreen} options={hide}/>
          <Stack.Screen name="Login" component={LoginScreen} options={hide}/>
          <Stack.Screen name="Register" component={RegisterScreen} options={hide}/>
          <Stack.Screen name="Home" component={Dashboard} options={hide}/>
          <Stack.Screen name="Chat" component={ChatScreen} options={hide}/>
          <Stack.Screen name="Contact" component={ContactScreen}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Router;