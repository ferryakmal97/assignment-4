import React, {Component} from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
import firestore from '@react-native-firebase/firestore';
import { BackgroundColor, PrimaryColor, SecondaryColor } from '../component/ThemeColors'
import * as Animatable from 'react-native-animatable';
import { setLogin, setUser } from '../redux/action/userAction';
import { getData, getUser, setData, setTarget } from '../redux/action/firestoreAction';

class ContactScreen extends Component{

    componentDidMount(){
        this.props.fetching('Users')
        this.props.gettingUser(this.props.user.user.uid)
    }
  
    signOut = () => {
        firestore()
        .collection('Users')
        .doc(this.props.user.user.uid)
        .update({
            tokens: firestore.FieldValue.delete(),
        });
        this.props.setLogin(false)
        this.props.setUser({})
        this.props.setData([])
        this.props.navigation.replace('Login')
    }
    
    toChat = (isi) => {
        this.props.setTarget(isi)
        this.props.navigation.navigate('Chat')
    }

    render(){
        return (
            <View style={styles.container}>
                {/* <Text style={{color:PrimaryColor}}>{JSON.stringify(this.props.isLogin)}</Text>
                <Text style={{color:PrimaryColor}}>{JSON.stringify(this.props.user)}</Text>
                <Text style={{color:PrimaryColor}}>{JSON.stringify(this.props.data)}</Text> */}
                {this.props.friends.map((isi,i)=>{
                return(
                    <View key={i} style={styles.friends} >
                        <TouchableOpacity onPress={()=> this.toChat(isi)}>
                            <View style={{flexDirection:'row'}}>
                                <View style={styles.pp} >
                                    <Image style={styles.photoUser} source={{uri: isi.photo}}/>
                                </View>
                                <View style={styles.tagLine}>
                                    <Text style={styles.name}>{isi.name}</Text>
                                    <Text>{isi.email}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                )
                })}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.userReducer.user,
        isLogin: state.userReducer.isLogin,
        friends: state.firestoreReducer.user,
        data: state.firestoreReducer.data
    }
}
  
  const mapDispatchToProps = (dispatch) => {
    return{
        setLogin : (data) => dispatch(setLogin(data)),
        setUser : (data) => dispatch(setUser(data)),
        fetching: (colName) => dispatch(getData(colName)),
        setData: (data) => dispatch(setData(data)),
        gettingUser: (userID) => dispatch(getUser(userID)),
        setTarget: (data) => dispatch(setTarget(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactScreen)

const styles = StyleSheet.create({
    container:{
      backgroundColor:'white',
      flex:1
    },
    header: {
        flexDirection:'row',
        alignItems:'center',
        alignSelf:'center',
        paddingHorizontal:10,
        paddingVertical:1,
        backgroundColor:BackgroundColor,
        elevation:10,
        width:'100%',
        borderBottomStartRadius:50,
        borderBottomEndRadius:50
    },
    textHeader: {
        color:PrimaryColor,
        fontSize:20
    },
    logOutButton: {
        backgroundColor:PrimaryColor,
        width:'70%',
        elevation:5,
        borderRadius:8,
        flexDirection:'row',
        justifyContent:'center',
        padding:3
    },
    friends: {
        flexDirection:'row',
        alignItems:'center',
        padding:10
    },
    photoUser:{
        width:'100%',
        height:'100%',
        borderRadius:100
    },
    pp:{
        width:70,
        height:70,
    },
    tagLine:{
        justifyContent:'space-between',
        margin:10
    },
    name: {
        fontStyle:'italic',
        fontSize:18
    },
    photoProfile: {
        width:100,
        height:100,
        borderRadius:100,
        margin:15
    }
})