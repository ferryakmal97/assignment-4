import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import {
  BackgroundColor,
  PrimaryColor,
  SecondaryColor,
} from '../component/ThemeColors';
import * as Animatable from 'react-native-animatable';

class WelcomeScreen extends Component {
  render() {
    return (
      <View style={styles.container}>

        <Animatable.Image
          source={require('../assets/fw.png')}
          style={styles.logoStyle}
          resizeMode='stretch'
          animation='fadeInDown'
          duration={2000}
        />
        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
            <TouchableOpacity onPress={()=>this.props.navigation.replace('Login')}>
                <Animatable.View animation='fadeInLeft' duration={2000} style={styles.box}>
                    <Text style={styles.text}>LOGIN</Text>
                </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.replace('Register')}>
                <Animatable.View animation='fadeInRight' duration={2000} style={styles.box}>
                    <Text style={styles.text}>REGISTER</Text>
                </Animatable.View>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default WelcomeScreen;

const {width, height} = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        backgroundColor: BackgroundColor,
        alignItems: 'center',
        justifyContent:'center',
        flex: 1,
    },
    box: {
        backgroundColor: SecondaryColor,
        borderWidth:4,
        borderColor:PrimaryColor,
        borderRadius: 25,
        paddingVertical: 15,
        marginHorizontal:10,
        width:width/2.5
    },
    logoStyle: {
        borderColor: PrimaryColor,
        width: width/2,
        height: height/3
    },
    text:{
        color:PrimaryColor,
        fontWeight:'bold',
        fontSize:25,
        alignSelf:'center'
    }
});
