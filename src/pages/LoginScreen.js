import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ToastAndroid, Dimensions} from 'react-native';
import InputText from '../component/InputText';
import Buttons from '../component/Buttons';
import {
  BackgroundColor,
  PrimaryColor,
  SecondaryColor,
} from '../component/ThemeColors';
import * as Animatable from 'react-native-animatable';
import {connect} from 'react-redux'
import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';
import { setUser, setLogin } from '../redux/action/userAction';

class LoginScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }

  async saveTokenToDatabase(token) {
    // Assume user is already signed in
    const userId = auth().currentUser.uid;
    console.log(userId)
    // Add the token to the users datastore
    await firestore()
      .collection('Users')
      .doc(userId)
      .update({
        tokens: firestore.FieldValue.arrayUnion(token),
      });
  }

  toast(msg) {
    ToastAndroid.showWithGravity(msg, ToastAndroid.SHORT, ToastAndroid.CENTER);
  }

  handleButtonSignIn = () => {
    const {email, password} = this.state;
    if (email === '' || password=== ''){
      this.toast('Data Belum Diinput')
    } else {
      auth().signInWithEmailAndPassword(email, password)
        .then((user) => {
          console.log(user)
          this.toast('Login Berhasil');
          this.props.setLogin(true)
          this.props.setUser(user)
          messaging()
          .getToken()
          .then(token => {
            return this.saveTokenToDatabase(token);
          });
          this.props.navigation.replace('Home')
        })
        .catch(error => { 
          if (error.code === 'auth/invalid-email') {
            this.toast('Email Tidak Ditemukan')
          }
          if (error.code === 'auth/wrong-password') {
            this.toast('Password Salah');
          }
          // console.error(error);
        });
    }
  }

  render() {
    return (
      <View style={styles.container}>

        <Animatable.Image
          source={require('../assets/fw.png')}
          style={styles.logoStyle}
          resizeMode='stretch'
          animation='fadeInDown'
          duration={2000}
        />
        <Animatable.View 
          style={styles.box} 
          animation='fadeInLeft'
          duration={1500}>
          <Animatable.View 
            animation={'fadeIn'}
            duration={1000}
            delay={1000}>
            <Text style={styles.textLogin}>Login</Text>

            <InputText
              iconName="user-alt"
              placeholder="Email"
              onChangeText={(value) => this.setState({email: value})}
            />
            <InputText
              iconName="key"
              password="password"
              placeholder="Password"
              onChangeText={(password) => this.setState({password})}
            />

            <View style={styles.textRegister}>
              <Text style={{color:'white'}}>Don't have an account? Sign Up</Text>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Register')}>
                <Text style={{color: PrimaryColor, fontWeight: 'bold'}}> Here</Text>
              </TouchableOpacity>
            </View>
          </Animatable.View>
        </Animatable.View>
        <Buttons 
          onPress={() => this.handleButtonSignIn()} 
          text="SIGN IN"
          animation={'fadeInUpBig'}
          duration={2000} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return{
    user: state.userReducer.user,
    isLogin: state.userReducer.isLogin,
    data: state.firestoreReducer.data
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    setLogin : (data) => dispatch(setLogin(data)),
    setUser : (data) => dispatch(setUser(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const {width, height} = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    backgroundColor: BackgroundColor,
    alignItems: 'center',
    flex: 1,
  },
  box: {
    backgroundColor: SecondaryColor,
    width: '90%',
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  logoStyle: {
    borderColor: PrimaryColor,
    width: width/2,
    height: height/3
  },
  textLogin: {
    fontWeight: 'bold',
    fontSize: 25,
    alignSelf: 'center',
    color: PrimaryColor,
    marginBottom: 10,
  },
  textRegister: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
});
