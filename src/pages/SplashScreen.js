import React, {Component} from 'react';
import { View, StyleSheet, Image, Dimensions} from 'react-native';
import { connect } from 'react-redux';
import { BackgroundColor, SecondaryColor } from '../component/ThemeColors';
import * as Animatable from 'react-native-animatable';
import auth from '@react-native-firebase/auth';

class SplashScreen extends Component {

    componentDidMount() {
        setTimeout(() => {
            auth().onAuthStateChanged((user)=>{
                console.log("user loggin:",user)
                if (!user) {
                  this.props.navigation.replace('Welcome');
                  // this.props.isOnline(false)
                }else{
                  this.props.navigation.replace('Home');
                }
            })
        }, 2000);
        // setTimeout (()=>{
        //     if(this.props.isLogin){
        //         this.props.navigation.replace('Home')
        //     } else {
        //         this.props.navigation.replace('Welcome')
        //     }
        //    },2000
        // )
    }
    
    render(){
        return(
            <View style={styles.container}>
                <Animatable.Image animation='fadeIn' duration={2000} style={styles.logo} source={require('../assets/fw.png')}/>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        isLogin: state.userReducer.isLogin
    }
}

export default connect(mapStateToProps)(SplashScreen)

const {width, height} = Dimensions.get('window')

const styles = StyleSheet.create({
    container:{
        backgroundColor:BackgroundColor,
        alignItems: 'center',
        justifyContent:"center",
        flex:1
    },
    logo:{
        width:width/1.2,
        height:height/2,
        resizeMode:'stretch'
    }
})