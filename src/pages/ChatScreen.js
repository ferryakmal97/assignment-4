import React, {Component} from 'react'
import { connect } from 'react-redux'
import {View, StyleSheet, Text, TouchableOpacity, Dimensions, TextInput, ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import ImageProfile from '../component/ImageProfile';
import { BackgroundColor, PrimaryColor, SecondaryColor } from '../component/ThemeColors'
import BubbleSend from '../component/BubbleSend';
import BubbleReceive from '../component/BubbleReceive';
import { setTarget, getChat } from '../redux/action/firestoreAction';
import firestore from '@react-native-firebase/firestore';
import axios from 'axios';

console.disableYellowBox = true 

class ChatScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
          text:'',
          CurrentDate:'',
          Hours:'',
          conversation:[]
        };
      }

      
    componentDidMount(){
        firestore()
            .collection('Chats')
            .doc(this.props.data.userId)
            .collection('TalkWith')
            .doc(this.props.target.userId)
            .onSnapshot( async documentSnapshot => {
                // console.log('User data: ', documentSnapshot.data());
                let tampung = await documentSnapshot.data()
                this.setState({
                    conversation: tampung.message
                })
            });
        // var date = new Date().getDate(); //Current Date
        // var month = new Date().getMonth() + 1; //Current Month
        // var year = new Date().getFullYear(); //Current Year
        var hours = new Date().getHours(); //Current Hours
        var min = new Date().getMinutes(); //Current Minutes
        // var sec = new Date().getSeconds(); //Current Seconds
        this.setState({
            Hours: hours + ':' + min
        })
    }

    _back = () => {
        this.props.setTarget({})
        this.props.navigation.goBack()
    }

    _scrollDown = ()=> { 
        this.scrollView.scrollToEnd({animated: false}); 
    } 

    sendButton = () => {
        let hours = new Date().getHours(); //Current Hours
        let min = new Date().getMinutes(); //Current Minutes
        this.setState({
            Hours: hours + ':' + min,
            text:''
        })
        firestore()
          .collection('Chats')
          .doc(this.props.data.userId)
          .collection('TalkWith')
          .doc(this.props.target.userId)
          .set({
            message: firestore.FieldValue.arrayUnion({
                text: this.state.text,
                hours: this.state.Hours,
                sendBy: this.props.data.userId}),
            last: {
                text:this.state.text,
                hours:this.state.Hours,
                photo:this.props.target.photo,
                name:this.props.target.name,
                photoU:this.props.data.photo,
                nameU:this.props.data.name,
                userId:this.props.target.userId
            }
          }, {merge:true})

        firestore()
          .collection('Chats')
          .doc(this.props.target.userId)
          .collection('TalkWith')
          .doc(this.props.data.userId)
          .set({
            message: firestore.FieldValue.arrayUnion({
                text: this.state.text,
                hours: this.state.Hours,
                sendBy: this.props.data.userId}),
            last: {
                text:this.state.text,
                hours:this.state.Hours,
                photo:this.props.data.photo,
                name:this.props.data.name,
                photoU:this.props.target.photo,
                nameU:this.props.target.name,
                userId:this.props.data.userId
            }
          }, {merge:true})

            const params = JSON.stringify({
                "registration_ids": this.props.target.tokens,
                "notification": {
                    "title": `Message from ${this.props.data.name}`,
                    "body": `${this.state.text}`,
                    "mutable_content": true,
                    "sound": "Tri-tone"
                }
            })
            const authkey = "key=AAAAK2vBBB0:APA91bFcgVtoXrk1Or1A0ayb25auMOrmApBStoO1l2EflLsJvELtL-lHR6YKOwHb19h1nZAHi8QqF10YeRoE63Yz58DQlxKMGiIzma5chnssxidHWnBw3J2R0U81epHT5is4fZreKIm6"
            axios.post('https://fcm.googleapis.com/fcm/send',params,{
                'headers':{
                    "Content-Type":"application/json",
                    "Authorization" : authkey
                }
            }).then(
                ()=>{},
                (er)=>{
                    console.log('notif:',er)
                }
            )
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.componentHeader}>
                        <TouchableOpacity>
                            <View style={styles.componentHeaderHeader}>
                                <Icon style={{fontSize:25}} name='arrow-left' onPress={()=> this._back()} />
                                <ImageProfile source={{uri: this.props.target.photo}} />    
                            </View>
                        </TouchableOpacity>
                        <View>
                            <Text>{this.props.target.name}</Text>
                            <Text>{this.props.target.isOnline?'Online':'Offline'}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.chatView}>
                    <ScrollView ref={(scroll) => {this.scrollView = scroll;}} onContentSizeChange={() => this._scrollDown()} > 
                        {this.state.conversation.map((item,i)=>{
                            if(item.sendBy === this.props.data.userId){
                                return(
                                    <BubbleSend key={i} value={[item.text]} time={[item.hours]} />
                                )
                            }else{
                                return(
                                    <BubbleReceive key={i} value={[item.text]} time={[item.hours]} />
                                )
                            }
                        })}
                        {/* <BubbleReceive/> */}
                    </ScrollView>
                </View>
                <View style={styles.componentHeader}>
                    <TextInput placeholder='Ketik Pesan' style={styles.inputan} value={this.state.text} onChangeText={(e)=>this.setState({
                        text : e
                    })}/>
                    <TouchableOpacity onPress={()=>this.sendButton()}>
                        <Icon style={styles.sendIcon} name='paper-plane'/> 
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.firestoreReducer.data,
        target: state.firestoreReducer.target,
        chat: state.firestoreReducer.chat
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        setTarget: (data) => dispatch(setTarget(data)),
        setChat: (docName, userId) => dispatch(getChat(docName, userId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen)

const {height, width} = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BackgroundColor,
        justifyContent:'space-between'
    },
    header:{
        backgroundColor:'white',
        height: height/10
    },
    componentHeader:{
        flexDirection:'row',
        padding:10,
        alignItems:'center'
    },
    componentHeaderHeader:{
        flexDirection:'row',
        alignItems:'center',
    },
    inputan:{
        flex:1,
        backgroundColor:'white',
        borderRadius:50,
        paddingHorizontal:15,
        marginRight:10
    },
    chatView:{
        flex:1
    },
    sendIcon:{
        fontSize:25,
        color:PrimaryColor
    }
})