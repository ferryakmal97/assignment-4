import React, {Component, Fragment} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import firestore from '@react-native-firebase/firestore';
import {
  BackgroundColor,
  PrimaryColor,
  SecondaryColor,
} from '../component/ThemeColors';
import * as Animatable from 'react-native-animatable';
import {setLogin, setUser} from '../redux/action/userAction';
import {
  getData,
  getUser,
  setData,
  setTarget,
  emptyChat,
} from '../redux/action/firestoreAction';
import Icon from 'react-native-vector-icons/MaterialIcons';
import messaging from '@react-native-firebase/messaging';
import { showMessage } from "react-native-flash-message";
import auth from '@react-native-firebase/auth';

class HomeScreen extends Component {
  constructor() {
    super();
    this.state = {
      conversation: [],
    };
  }

  componentDidMount() {
    this.props.fetching('Users');
    firestore().collection('Users').doc(this.props.data.userId).update({
      isOnline:true
    })
    this.props.gettingUser(this.props.user.user.uid);
    firestore()
      .collection('Chats')
      .doc(this.props.data.userId)
      .collection('TalkWith')
      .onSnapshot((v) => {
        let isiData = [];
        v.docs.forEach((array) => {
          isiData.push(array.data().last);
          this.setState({
            conversation: isiData,
          });
        });
      });
    this.messaging()
    // .doc(this.props.target.userId)
    // .onSnapshot( async documentSnapshot => {
    //     // console.log('User data: ', documentSnapshot.data());
    //     let tampung = await documentSnapshot.data()
    //     console.log("chat nih",tampung)
    //     this.setState({
    //         conversation: tampung.message
    //     })
    // });
  }

  componentWillUnmount(){
    firestore().collection('Users').doc(this.props.data.userId).update({
      isOnline:false
    })
  }

  messaging = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      messaging().onMessage(async (remoteMessage) => {
        setTimeout(() => {
          showMessage({
            message: remoteMessage.notification.title,
            description: remoteMessage.notification.body,
            backgroundColor: SecondaryColor, // background color
            color: PrimaryColor
          });
        }, 300);
      });
    } else {
      null;
    }
  };


  signOut = () => {
    firestore().collection('Users').doc(this.props.user.user.uid).update({
      tokens: firestore.FieldValue.delete(),
    });
    auth()
    .signOut()
    .then(() => {
      console.log('User signed out!')
      // navigation handle by observer onAuthStateChange at splashscreen
      // navigation.replace("Login")
      ,
      (error) => {
        showMessage("failed to logout")
        console.log("error logout:", error)
      }
    });
    this.props.setLogin(false);
    this.props.setUser({});
    this.props.setData([]);
    this.props.navigation.replace('Login');
  };

  toChat = (userId) => {
    this.props.setTarget(userId);
    this.props.navigation.navigate('Chat');
  };

  render() {
    return (
      <View style={styles.container}>
        <Animatable.View
          style={styles.header}
          animation="fadeInDownBig"
          duration={1500}>
          <Image
            style={styles.photoProfile}
            source={{uri: this.props.data.photo}}
          />
          <View>
            <Text style={styles.textHeader}>{this.props.data.name}</Text>
            <View>
              <TouchableOpacity>
                <Animatable.View style={styles.logOutButton}>
                  <Text
                    style={{fontWeight: 'bold'}}
                    onPress={() => this.signOut()}>
                    LOGOUT
                  </Text>
                </Animatable.View>
              </TouchableOpacity>
            </View>
          </View>
        </Animatable.View>
        <ScrollView>
          {/* <Text>{JSON.stringify(this.state.conversation)}</Text> */}
          {this.state.conversation.length != 0 && (
            <Fragment>
              {this.state.conversation.map((isi, i) => {
                return (
                  <View key={i} style={styles.friends}>
                      {isi != null && (
                        <TouchableOpacity onPress={() => this.toChat(isi.userId)}>
                        <View style={{flexDirection: 'row'}}>
                            <Image style={styles.photoUser} source={{uri: isi.photo}} />
                            <View style={styles.tagLine}>
                            <Text style={styles.name}>{isi.name}</Text>
                            <Text>{isi.text}</Text>
                            </View>
                            <Text style={{alignItems: 'flex-end'}}>
                            {isi.hours}
                            </Text>
                        </View>
                        </TouchableOpacity>
                      )}
                      {isi == null && (
                            <View
                            style={{
                                borderBottomColor: 'black',
                                borderBottomWidth: 5
                            }}
                            />
                      )}
                  </View>
                );
              })}
            </Fragment>
          )}
        </ScrollView>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate('Contact')}>
          <Icon style={styles.sendIcon} name='add-circle'/> 
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer.user,
    isLogin: state.userReducer.isLogin,
    friends: state.firestoreReducer.user,
    data: state.firestoreReducer.data,
    target: state.firestoreReducer.target,
    chat: state.firestoreReducer.chat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLogin: (data) => dispatch(setLogin(data)),
    setUser: (data) => dispatch(setUser(data)),
    fetching: (colName) => dispatch(getData(colName)),
    setData: (data) => dispatch(setData(data)),
    gettingUser: (userID) => dispatch(getUser(userID)),
    setTarget: (userId) => dispatch(setTarget(userId)),
    emptyChat: (data) => dispatch(emptyChat(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
    paddingHorizontal: 10,
    paddingVertical: 1,
    backgroundColor: BackgroundColor,
    elevation: 10,
    width: '100%',
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
  },
  textHeader: {
    color: PrimaryColor,
    fontSize: 20,
  },
  logOutButton: {
    backgroundColor: PrimaryColor,
    width: '70%',
    elevation: 5,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 3,
  },
  friends: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    width: width,
  },
  photoUser: {
    width: 70,
    height: 70,
    borderRadius: 100,
  },
  tagLine: {
    justifyContent: 'space-between',
    margin: 10,
    width: width / 1.6,
  },
  name: {
    fontStyle: 'italic',
    fontSize: 18,
  },
  photoProfile: {
    width: 100,
    height: 100,
    borderRadius: 100,
    margin: 15,
  },
  sendIcon:{
    fontSize:50,
    color:PrimaryColor,
    alignSelf:'flex-end',
    margin:20
  }
});
