import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ToastAndroid,
  ScrollView,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import Buttons from '../component/Buttons';
import InputText from '../component/InputText';
import {connect} from 'react-redux'
import { addData} from '../redux/action/userAction';
import * as Animatable from 'react-native-animatable';
import { BackgroundColor, PrimaryColor, SecondaryColor } from '../component/ThemeColors';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

class RegisterScreen extends Component {
  constructor(){
    super()
    this.state = {
      fullName:'',
      phone:'',
      password:'',
      email:'',
    }
  }

  toast(msg) {
    ToastAndroid.showWithGravity(msg, ToastAndroid.SHORT, ToastAndroid.CENTER);
  }

  _handleButtonSignUp = () => {
    const {fullName, phone, password, email} = this.state
    auth().createUserWithEmailAndPassword(email, password).then(
      (ressponse)=>{
        console.log(ressponse)
        firestore()
          .collection('Users')
          .doc(ressponse.user.uid)
          .set({
            name: fullName,
            phone: phone,
            email: email,
            userId: ressponse.user.uid
          })
          .then(() => {
            console.log('User added!');
          });
          this.toast('Registrasi Berhasil');
          this.props.navigation.replace('Login')
      },
      (error)=>{
        console.log(error)
        this.toast('Registrasi Gagal');
      }
    )
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Animatable.Image resizeMode='stretch' animation='fadeInDownBig' duration={2000} source={require('../assets/fw.png')} style={styles.logoStyle}/>
          <Animatable.View style={styles.box} animation='fadeInLeft'
          duration={1500} >
            <Animatable.View animation='fadeIn' duration={1000} delay={1000}>
              <Text style={styles.textRegister}>Register</Text>

              <InputText iconName="user-alt" placeholder='Full Name' onChangeText={(e) => this.setState({fullName:e})} />

              <InputText iconName="phone" keyboardType='phone-pad' placeholder="Phone Number" onChangeText={(e) => this.setState({phone:e})} />

              <InputText iconName="at" placeholder="Email" onChangeText={(e) => this.setState({email:e})} />

              <InputText iconName="key" password="password" placeholder="password" onChangeText={(e) => this.setState({password:e})} />

              <View style={styles.textLogin}>
                <Text style={{color:'white'}}>Already have an account? Sign In</Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Login')}>
                  <Text style={{color: PrimaryColor, fontWeight: 'bold'}}> Here</Text>
                </TouchableOpacity>
              </View>
            </Animatable.View>
          </Animatable.View>
          <Buttons 
            text="SIGN UP" 
            onPress={() => this._handleButtonSignUp()} 
            animation={'fadeInUpBig'}
            duration={2000} />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  return{
    users: state.users,
    user: state.username,
    pass: state.password,
    email: state.email,
    fullName: state.fullName
  }
}

const mapDispatchToProps = (dispatch) => {
  return{
    addData : (data) => dispatch(addData(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: BackgroundColor,
    alignItems: 'center',
    height: height,
  },
  box: {
    backgroundColor: SecondaryColor,
    width: '90%',
    borderRadius: 50,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  logoStyle: {
    marginTop:-30,
    borderColor: PrimaryColor,
    width: width/2,
    height: height/3
  },
  textRegister: {
    fontWeight: 'bold',
    fontSize: 25,
    alignSelf: 'center',
    color: PrimaryColor,
    marginBottom: 10,
  },
  textLogin: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
});
